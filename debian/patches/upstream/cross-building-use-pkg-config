Description: fails finding libmysqlclient when cross building
Author: Helmut Grohne <helmut@subdivi.de>
Bug-Debian: 1019395
Last-Update: 2022-09-07

bacula fails to cross build from source, because it fails finding
libmysqlclient. It looks for mysql libraries using mysql_config, which
is known to not work during cross compilation. Rather than mysql_config,
using pkg-config works. This patch introduces another way of detecting
mysql using pkg-config as pkg-config generally works with cross
compilation and mysql provides a working .pc files.

--- bacula-9.6.7.orig/autoconf/bacula-macros/db.m4
+++ bacula-9.6.7/autoconf/bacula-macros/db.m4
@@ -340,21 +340,31 @@
 AC_HELP_STRING([--with-mysql@<:@=DIR@:>@], [Include MySQL support. DIR is the MySQL base install directory, default is to search through a number of common places for the MySQL files.]),
 [
   HAVE_LIBSR="no"
-  if test "$withval" != "no"; then
-     if test "$withval" = "yes"; then
-        MYSQL_CONFIG=`which mysql_config 2>/dev/null`
-        if test "x${MYSQL_CONFIG}" != x; then
-           MYSQL_BINDIR="${MYSQL_CONFIG%/*}"
-           ${MYSQL_CONFIG} --libs_r >/dev/null 2>&1
-           if test $? = 0; then
-              MYSQL_LIBDIR=`${MYSQL_CONFIG} --libs_r`
-              MYSQL_INCDIR=`${MYSQL_CONFIG} --include`
-              HAVE_LIBSR="yes"
-           else
-              ${MYSQL_CONFIG} --variable=pkglibdir > /dev/null 2>&1
-              if test $? = 0 ; then
-                 MYSQL_LIBDIR=`${MYSQL_CONFIG} --variable=pkglibdir`
-                 MYSQL_INCDIR=`${MYSQL_CONFIG} --variable=pkgincludedir`
+  AS_IF([test "$withval" != "no"],[
+     AS_IF([test "$withval" = "yes"],[
+        AC_REQUIRE([PKG_PROG_PKG_CONFIG])
+        if $PKG_CONFIG --exists mysqlclient; then
+           MYSQL_BINDIR=/usr/bin
+           MYSQL_LIBDIR=`$PKG_CONFIG mysqlclient --libs`
+           MYSQL_INCDIR=`$PKG_CONFIG mysqlclient --cflags-only-I`
+           HAVE_LIBSR=yes
+        fi
+        # if something wrong fall back to old method
+        if test "x${MYSQL_LIBDIR}" = x -o "x${MYSQL_INCDIR}" = x ; then
+           MYSQL_CONFIG=`which mysql_config 2>/dev/null`
+           if test "x${MYSQL_CONFIG}" != x; then
+              MYSQL_BINDIR="${MYSQL_CONFIG%/*}"
+              ${MYSQL_CONFIG} --libs_r >/dev/null 2>&1
+              if test $? = 0; then
+                 MYSQL_LIBDIR=`${MYSQL_CONFIG} --libs_r`
+                 MYSQL_INCDIR=`${MYSQL_CONFIG} --include`
+                 HAVE_LIBSR="yes"
+              else
+                 ${MYSQL_CONFIG} --variable=pkglibdir > /dev/null 2>&1
+                 if test $? = 0 ; then
+                    MYSQL_LIBDIR=`${MYSQL_CONFIG} --variable=pkglibdir`
+                    MYSQL_INCDIR=`${MYSQL_CONFIG} --variable=pkgincludedir`
+                 fi
               fi
            fi
         fi
@@ -419,7 +429,7 @@
               AC_MSG_ERROR(Unable to find mysql.h in standard locations)
            fi
         fi
-     else
+     ],[
         if test -f $withval/include/mysql/mysql.h; then
            MYSQL_INCDIR=$withval/include/mysql
            if test -f $withval/lib64/mysql/libmysqlclient_r.a \
@@ -478,7 +488,7 @@
            AC_MSG_RESULT(no)
            AC_MSG_ERROR(Invalid MySQL directory $withval - unable to find mysql.h under $withval)
         fi
-     fi
+     ])
      if test "x${MYSQL_LIBDIR}" != x; then
         MYSQL_INCLUDE=-I$MYSQL_INCDIR
         if test "x$HAVE_LIBSR" = "xyes"; then
@@ -546,7 +556,7 @@
      else
         AC_MSG_RESULT(no)
      fi
-  fi
+  ])
 ],[
   AC_MSG_RESULT(no)
 ])
