Description: Adapt upstream systemd units to Debian packaging 
Author: Sven Hartge <sven@svenhartge.de>
Forwarded: not-needed

--- a/platforms/systemd/bacula-dir.service.in
+++ b/platforms/systemd/bacula-dir.service.in
@@ -3,32 +3,39 @@
 # Copyright (C) 2000-2022 Kern Sibbald
 # License: BSD 2-Clause; see file LICENSE-FOSS
 #
-# /lib/systemd/system/bacula-dir.service
+# /lib/systemd/system/bacula-director.service
 #
 # Description:
-#    Used to start/stop/reload the bacula director daemon service (bacula-dir)
+#    Used to start/stop/reload the bacula director daemon service (bacula-director)
 #
-#    enable : systemctl enable bacula-dir
-#    start : systemctl start bacula-dir
+#    enable : systemctl enable bacula-director
+#    start : systemctl start bacula-director
 #
 #
 
 # From http://www.freedesktop.org/software/systemd/man/systemd.unit.html
 [Unit]
 Description=Bacula Director Daemon service
+Documentation=man:bacula-dir(8)
 Requires=network.target
-After=network.target multi-user.target
+After=network.target multi-user.target postgresql.service mysql.service bacula-fd.service bacula-sd.service
 RequiresMountsFor=@working_dir@ @sysconfdir@ @sbindir@
 
 # From http://www.freedesktop.org/software/systemd/man/systemd.service.html
 [Service]
-Type=simple
-User=@dir_user@
-Group=@dir_group@
-ExecStart=@sbindir@/bacula-dir -fP -c @sysconfdir@/bacula-dir.conf
-ExecReload=@sbindir@/bacula-dir -t -c @sysconfdir@/bacula-dir.conf
+Type=exec
+User=bacula
+Group=bacula
+Environment="CONFIG=/etc/bacula/bacula-dir.conf"
+EnvironmentFile=-/etc/default/bacula-dir
+ExecStartPre=@sbindir@/bacula-dir -t -c $CONFIG
+ExecStart=@sbindir@/bacula-dir -fP -c $CONFIG
+ExecReload=@sbindir@/bacula-dir -t -c $CONFIG
 ExecReload=/bin/kill -HUP $MAINPID
 SuccessExitStatus=15
+Restart=on-failure
+RestartSec=60
 
 [Install]
 WantedBy=multi-user.target
+Alias=bacula-dir.service
--- a/platforms/systemd/bacula-fd.service.in
+++ b/platforms/systemd/bacula-fd.service.in
@@ -16,17 +16,25 @@
 # from http://www.freedesktop.org/software/systemd/man/systemd.unit.html
 [Unit]
 Description=Bacula File Daemon service
+Documentation=man:bacula-fd(8)
 Requires=network.target
 After=network.target
 RequiresMountsFor=@working_dir@ @sysconfdir@ @sbindir@
 
 # from http://www.freedesktop.org/software/systemd/man/systemd.service.html
 [Service]
-Type=simple
-User=@fd_user@
-Group=@fd_group@
-ExecStart=@sbindir@/bacula-fd -fP -c @sysconfdir@/bacula-fd.conf
+Type=exec
+User=root
+Group=root
+Environment="CONFIG=/etc/bacula/bacula-fd.conf"
+EnvironmentFile=-/etc/default/bacula-fd
+ExecStartPre=@sbindir@/bacula-fd -t -c $CONFIG
+ExecStart=@sbindir@/bacula-fd -fP -c $CONFIG
+ExecReload=@sbindir@/bacula-fd -t -c $CONFIG
+ExecReload=/bin/kill -HUP $MAINPID
 SuccessExitStatus=15
+Restart=on-failure
+RestartSec=60
 StandardOutput=journal
 StandardError=journal
 
--- a/platforms/systemd/bacula-sd.service.in
+++ b/platforms/systemd/bacula-sd.service.in
@@ -15,20 +15,25 @@
 # from http://www.freedesktop.org/software/systemd/man/systemd.unit.html
 [Unit]
 Description=Bacula Storage Daemon service
+Documentation=man:bacula-sd(8)
 Requires=network.target
 After=network.target
 RequiresMountsFor=@working_dir@ @sysconfdir@ @sbindir@ @piddir@
 
 # from http://www.freedesktop.org/software/systemd/man/systemd.service.html
 [Service]
-Type=forking
-User=@sd_user@
-Group=@sd_group@
-SupplementaryGroups=@sd_user@
+Type=exec
+User=bacula
+Group=tape
+SupplementaryGroups=bacula
+Environment="CONFIG=/etc/bacula/bacula-sd.conf"
+EnvironmentFile=-/etc/default/bacula-sd
 Environment="LD_LIBRARY_PATH=@libdir@"
-# comment out to get traces working at startup using -T and -d options
-#WorkingDirectory=@working_dir@
-ExecStart=@sbindir@/bacula-sd -dt -c @sysconfdir@/bacula-sd.conf
+ExecStartPre=@sbindir@/bacula-sd -t -c $CONFIG
+ExecStart=@sbindir@/bacula-sd -fP -c $CONFIG
+ExecReload=@sbindir@/bacula-sd -t -c $CONFIG
+ExecReload=/bin/kill -HUP $MAINPID
+SuccessExitStatus=15
 StandardOutput=journal
 StandardError=journal
 TimeoutStopSec=3min
@@ -38,5 +43,7 @@
 OOMScoreAdjust=-1000
 AmbientCapabilities=CAP_LINUX_IMMUTABLE
 
+Restart=on-failure
+RestartSec=60
 [Install]
 WantedBy=multi-user.target
