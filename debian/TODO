After Bullseye+1:

 * remove Breaks/Replaces (<< 9.6.4-1)

After Bullseye:

 * remove symlinks for b*json utils from /usr/lib/bacula
   (hint: bacula-{director,sd,fd,console}.links)
 * remove Breaks/Replaces (<< 7.4.3+dfsg-3)

v9.4.0

 * src/lib/Makefile *_test installs unit tests -> install somewhere (new package?) and use for autopkgtest
   - bsock test -> depends on /bin/netcat

Normal:

 * user creation/locking is suboptimal (bacula-common.preinst)
   check dh-sysuser
   https://lists.debian.org/debian-devel/2016/10/msg00665.html
   https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=621833

Wishlist:

 * autopkgtests

   binaries not yet tested:

   bacula-bscan: /usr/sbin/bscan
   bacula-common: /usr/libexec/bacula/btraceback
   bacula-common: /usr/lib/nagios/plugins/check_bacula
   bacula-common: /usr/sbin/bsmtp
   bacula-console-qt: /usr/sbin/bat
   bacula-director: /usr/sbin/dbcheck
   bacula-sd: /usr/sbin/bcopy
   bacula-sd: /usr/sbin/bextract
   bacula-sd: /usr/sbin/bls
   bacula-sd: /usr/sbin/btape
   bacula-tray-monitor: /usr/sbin/bacula-tray-monitor

 * convert debian/copyright into machine readable format
   see: https://wiki.debian.org/UscanEnhancements

 * autopostrm would ideally become a debhelper script ...

 * Consider tending to users not using dbconfig-common
   see 2nd half of http://lists.alioth.debian.org/pipermail/pkg-bacula-devel/2016-July/001520.html

 * Reactivate patches for baculabackupreport.in and continue work on them
   finish work on baculabackupreport script:
   Still missing:
     - Automatic handling of the database password
     - modifying the debian placeholders in postinst
     - handling mysql vs. mariadb
     - mysql needs "-p" before the password, mariadb not (-> upstream)

Problems:

 * dpkg --status bacula-director-(DBTYPE) lists conffiles as obsolete
   that moved to bacula-director. We probably have to live with that
   as moving conffiles between packages isn't well supported.
   See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=595112 - but
   this doesn't even take a move from dpkg- to ucf-handled conffiles
   into account

Review unmerged old changes:

 * Ask for passwords via debconf (git tag development/2011-04-29)
    d4676fc19978a0588bb09a58234cf451d15859d6 Don't set passwords at build time but ask for them
    aea2145a4f135fa6d18d33384a1af2c2bffc8617 Get rid of useless common-functions

Hints

 Bacula packaging in other distributions:
 * Fedora/Red Hat: https://src.fedoraproject.org/rpms/bacula/tree/master
 * Gentoo: https://gitweb.gentoo.org/repo/gentoo.git/tree/app-backup/bacula/
 * Arch Linux: https://aur.archlinux.org/pkgbase/bacula
 * OpenBSD: https://cvsweb.openbsd.org/cgi-bin/cvsweb/ports/sysutils/bacula/
 * FreeBSD: https://svnweb.freebsd.org/ports/head/sysutils/bacula9-server/
