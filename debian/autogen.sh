#!/bin/sh

(aclocal --version) < /dev/null > /dev/null 2>&1 || {
  echo
  echo "** Error **: You must have aclocal installed."
  echo
  exit 1
}

(autoconf --version) < /dev/null > /dev/null 2>&1 || {
  echo
  echo "** Error **: You must have autoconf installed."
  echo
  exit 1
}

echo aclocal
(cd autoconf; aclocal -I bacula-macros/ -I gettext-macros/ -I libtool/)
echo autoconf
autoconf -I autoconf/ -o configure autoconf/configure.in

