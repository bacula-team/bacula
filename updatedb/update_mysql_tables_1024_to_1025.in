#!/bin/sh
#
# Copyright (C) 2000-2021 Kern Sibbald
# License: BSD 2-Clause; see file LICENSE-FOSS
#
# Shell script to update MySQL
#
echo " "
echo "This script will update a Bacula MySQL database from version 1024 to 1025"
echo " "
bindir=@MYSQL_BINDIR@
PATH="$bindir:$PATH"
db_name=${db_name:-@db_name@}

mysql $* -D ${db_name} -e "select VersionId from Version\G" >/tmp/$$
DBVERSION=`sed -n -e 's/^VersionId: \(.*\)$/\1/p' /tmp/$$`
if [ $DBVERSION != 1024 ] ; then
   echo " "
   echo "The existing database is version $DBVERSION !!"
   echo "This script can only update an existing version 1024 database to version 1025."
   echo "Error. Cannot upgrade this database."
   echo " "
   exit 1
fi

if mysql $* -f  <<END-OF-DATA
USE ${db_name};
ALTER TABLE Client ADD COLUMN Plugins VARCHAR(254) DEFAULT '';

-- --------------------------------------------------------------
-- MetaData Index

CREATE TABLE MetaEmail
(
    EmailPKId  BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, -- Used for replication
    EmailTenant     		VARCHAR(255),
    EmailOwner       		VARCHAR(255),
    EmailId          		VARCHAR(255),
    EmailTime         		DATETIME,
    EmailTags         		TEXT,
    EmailSubject      		TEXT,
    EmailFolderName   		TEXT,
    EmailFrom         		VARCHAR(255),
    EmailTo           		TEXT,
    EmailCc           		TEXT,
    EmailInternetMessageId 	VARCHAR(255),
    EmailBodyPreview  		TEXT,
    EmailImportance   		VARCHAR(255),
    EmailConversationId    	VARCHAR(255),
    EmailIsRead       		TINYINT,
    EmailIsDraft      		TINYINT,
    EmailHasAttachment 		TINYINT,
    EmailSize	 			INTEGER,
    Plugin       			VARCHAR(255),
    FileIndex   	 		INTEGER,
    JobId        			INTEGER,
    PRIMARY KEY(EmailPKId)
);

-- Requires TEXT instead of BLOB
CREATE FULLTEXT INDEX meta_emailsubjectbody ON MetaEmail 
       (EmailSubject, EmailBodyPreview, EmailTo, EmailCc, EmailFrom);
CREATE INDEX meta_emailowner ON MetaEmail (EmailTenant(255), EmailOwner(255));
CREATE INDEX meta_emailtime on MetaEmail (EmailTime);
CREATE INDEX meta_emailtags on MetaEmail (EmailTags(255));
CREATE INDEX meta_emailfoldername on MetaEmail (EmailFolderName(255));
CREATE INDEX meta_emailconversationid on MetaEmail (EmailConversationId(255));
CREATE INDEX meta_emailisread on MetaEmail (EmailIsRead);
CREATE INDEX meta_emailhasattachment on MetaEmail (EmailHasAttachment);
CREATE INDEX meta_emailjobid on MetaEmail (Jobid);

CREATE TABLE MetaAttachment
(
    AttachmentPKId  BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, -- Used for replication
    AttachmentTenant            VARCHAR(255),
    AttachmentOwner    		VARCHAR(255),
    AttachmentName     		TEXT,
    AttachmentEmailId  		VARCHAR(255),
    AttachmentContentType 	VARCHAR(255),
    AttachmentIsInline 		SMALLINT,
    AttachmentSize	 		INTEGER,
    Plugin      			VARCHAR(255),
    FileIndex    			INTEGER,
    JobId        			INTEGER,
    PRIMARY KEY(AttachmentPKId)
);

CREATE INDEX meta_attachmentowner ON MetaAttachment (AttachmentTenant,AttachmentOwner);
CREATE INDEX meta_attachmentemailid ON MetaAttachment (AttachmentEmailId);
CREATE INDEX meta_attachmentjobid on MetaAttachment (Jobid);

INSERT INTO Status (JobStatus,JobStatusLong,Severity) VALUES
   ('l', 'Doing data despooling',15),
   ('L', 'Committing data (last despool)',15),
   ('u', 'Cloud upload',15),
   ('w', 'Cloud download',15),
   ('q', 'Queued waiting for device',15),
   ('W', 'Terminated normally with warnings',25);

ALTER TABLE RestoreObject MODIFY ObjectName MEDIUMBLOB;
ALTER TABLE RestoreObject MODIFY PluginName BLOB;
ALTER TABLE Object MODIFY PluginName BLOB;

UPDATE Version SET VersionId=1025;
END-OF-DATA
then
   echo "Update of Bacula MySQL tables 1024 to 1025 succeeded."
   getVersion
else
   echo "Update of Bacula MySQL tables 1024 to 1025 failed."
   exit 1
fi

exit 0
