#
# Makefile for building FD plugins PluginLibrary for Bacula
#
# Copyright (C) 2000-2023 Kern Sibbald
# License: BSD 2-Clause; see file LICENSE-FOSS
#
#  Author: Radoslaw Korzeniewski, radoslaw@korzeniewski.net
#

@MCOMMON@

.PHONY: all
first_rule: all

topdir = @BUILD_DIR@
working_dir = @working_dir@
LIBTOOL_CLEAN_TARGET = @LIBTOOL_CLEAN_TARGET@
LIBTOOL_UNINSTALL_TARGET = @LIBTOOL_UNINSTALL_TARGET@

SYSCONFDIR=@sysconfdir@

_GIT := $(shell eval $(topdir)/scripts/getgitcommit)
VERSIONGIT = "/git-$(_GIT)"

SYBASE_TMPDIR = @sybase_tmpdir@
SYBASE_CONFIG = @sysconfdir@/sbt.conf
SBTCONFIG = @sysconfdir@/sbt.conf
ORACLE_TMPDIR = @oracle_tmpdir@
SAP_TMPDIR = @sap_tmpdir@
RMAN_SCRIPT_DIR = @sysconfdir@
RSYNC_INC = @RSYNC_INC@
RSYNC_LIBS = @RSYNC_LIBS@
LDAP_LIBS = @LDAP_LIBS@
LDAP_LDFLAGS = @LDAP_LDFLAGS@
LDAP_INC = @LDAP_INC@
FD_LDAP_TARGET = @FD_LDAP_TARGET@
FD_LDAP_TARGET_INSTALL = @FD_LDAP_TARGET_INSTALL@

SRCDIR = $(topdir)/src
FDDIR = $(SRCDIR)/filed
LIBDIR = $(SRCDIR)/lib
FINDLIBDIR = $(SRCDIR)/findlib
FDPLUGDIR = $(SRCDIR)/plugins/fd
PLUGINLIBDIR = $(FDPLUGDIR)/pluginlib

UNITTESTSOBJ = $(LIBDIR)/unittests.lo
INIOBJ = $(LIBDIR)/ini.lo
LIBBACOBJ = $(LIBDIR)/libbac.la
LIBBACCFGOBJ = $(LIBDIR)/libbaccfg.la
INIOBJ = $(LIBDIR)/ini.lo
METAPLUGINOBJ = $(PLUGINLIBOBJ) $(PLUGINLIBDIR)/ptcomm.lo $(PLUGINLIBDIR)/metaplugin.lo $(PLUGINLIBDIR)/metaplugin_attributes.lo $(PLUGINLIBDIR)/metaplugin_accurate.lo $(PLUGINLIBDIR)/metaplugin_metadata.lo
PLUGINLIBOBJ = $(PLUGINLIBDIR)/pluginlib.lo
PLUGINBASE = $(PLUGINLIBOBJ) $(PLUGINLIBDIR)/pluginclass.lo $(PLUGINLIBDIR)/pluginbase.lo $(PLUGINLIBDIR)/pluginctx.lo

EXTRA_INSTALL_TARGET = @FD_PLUGIN_INSTALL@

.SUFFIXES:    .c .cpp   .lo

$(UNITTESTSOBJ):
	$(MAKE) -C $(LIBDIR) unittests.lo

$(INIOBJ):
	$(MAKE) -C $(LIBDIR) ini.lo

$(LIBBACOBJ):
	$(MAKE) -C $(LIBDIR) libbac.la

$(LIBBACCFGOBJ):
	$(MAKE) -C $(LIBDIR) libbaccfg.la

$(PLUGINLIBDIR)/test_metaplugin_backend.lo:
	$(MAKE) -C $(PLUGINLIBDIR) test_metaplugin_backend.lo

$(PLUGINLIBDIR)/pluginlib.lo: $(PLUGINLIBDIR)/pluginlib.cpp $(PLUGINLIBDIR)/pluginlib.h
	$(MAKE) -C $(PLUGINLIBDIR) pluginlib.lo

$(PLUGINLIBDIR)/ptcomm.lo: $(PLUGINLIBDIR)/ptcomm.cpp $(PLUGINLIBDIR)/ptcomm.h
	$(MAKE) -C $(PLUGINLIBDIR) ptcomm.lo

$(PLUGINLIBDIR)/metaplugin.lo: $(PLUGINLIBDIR)/metaplugin.cpp $(PLUGINLIBDIR)/metaplugin.h
	$(MAKE) -C $(PLUGINLIBDIR) metaplugin.lo

$(PLUGINLIBDIR)/metaplugin_attributes.lo: $(PLUGINLIBDIR)/metaplugin_attributes.cpp $(PLUGINLIBDIR)/metaplugin_attributes.h
	$(MAKE) -C $(PLUGINLIBDIR) metaplugin_attributes.lo

$(PLUGINLIBDIR)/metaplugin_accurate.lo: $(PLUGINLIBDIR)/metaplugin_accurate.cpp $(PLUGINLIBDIR)/metaplugin_accurate.h
	$(MAKE) -C $(PLUGINLIBDIR) metaplugin_accurate.lo

$(PLUGINLIBDIR)/metaplugin_metadata.lo: $(PLUGINLIBDIR)/metaplugin_metadata.cpp $(PLUGINLIBDIR)/metaplugin_metadata.h
	$(MAKE) -C $(PLUGINLIBDIR) metaplugin_metadata.lo

$(PLUGINLIBDIR)/iso8601.lo: $(PLUGINLIBDIR)/iso8601.cpp $(PLUGINLIBDIR)/iso8601.h
	$(MAKE) -C $(PLUGINLIBDIR) iso8601.lo

$(PLUGINLIBDIR)/execprog.lo: $(PLUGINLIBDIR)/execprog.cpp $(PLUGINLIBDIR)/execprog.h
	$(MAKE) -C $(PLUGINLIBDIR) execprog.lo

$(PLUGINLIBDIR)/pluginclass.lo: $(PLUGINLIBDIR)/pluginclass.cpp $(PLUGINLIBDIR)/pluginclass.h
	$(MAKE) -C $(PLUGINLIBDIR) pluginclass.lo

$(PLUGINLIBDIR)/pluginbase.lo: $(PLUGINLIBDIR)/pluginbase.cpp $(PLUGINLIBDIR)/pluginbase.h
	$(MAKE) -C $(PLUGINLIBDIR) pluginbase.lo

$(PLUGINLIBDIR)/pluginctx.lo: $(PLUGINLIBDIR)/pluginctx.cpp $(PLUGINLIBDIR)/pluginctx.h
	$(MAKE) -C $(PLUGINLIBDIR) pluginctx.lo
